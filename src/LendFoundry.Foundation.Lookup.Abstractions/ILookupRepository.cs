﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.Foundation.Lookup
{
    public interface ILookupRepository : IRepository<ILookupEntry>
    {
        IEnumerable<ILookupEntry> Add(string entity, IDictionary<string, string> lookupEntries);

        bool Delete(string entity, string code);

        bool DeleteAll(string entity);

        IEnumerable<ILookupEntry> GetLookupEntries(string entity);

        ILookupEntry GetLookupEntry(string entity, string code);

        List<string> GetEntities();
    }
}