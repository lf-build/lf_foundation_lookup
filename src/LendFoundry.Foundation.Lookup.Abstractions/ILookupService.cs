﻿using System.Collections.Generic;

namespace LendFoundry.Foundation.Lookup
{
    public interface ILookupService
    {
        IEnumerable<ILookupEntry> Add(string entity, IDictionary<string, string> lookupEntries);

        void Delete(string entity, string code);

        void DeleteAll(string entity);

        Dictionary<string, string> GetLookupEntries(string entity);

        Dictionary<string, string> GetLookupEntry(string entity, string code);

        List<string> GetEntities();

        Dictionary<string, IEnumerable<ILookupEntry>> GetMultipleLookupEntries(List<string> entities);

        bool Exist(string entity, string code);
    }
}