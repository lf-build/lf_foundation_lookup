﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Foundation.Lookup
{
    public interface ILookupEntry : IAggregate
    {
        string Entity { get; set; }

        string Code { get; set; }

        string Value { get; set; }
    }
}