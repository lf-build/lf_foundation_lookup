﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Foundation.Lookup
{
    public class LookupEntry : Aggregate, ILookupEntry
    {
        public string Entity { get; set; }

        public string Code { get; set; }

        public string Value { get; set; }
    }
}