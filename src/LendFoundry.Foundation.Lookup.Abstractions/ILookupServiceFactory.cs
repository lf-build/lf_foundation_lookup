﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Foundation.Lookup
{
    public interface ILookupServiceFactory
    {
        ILookupService Create(ITokenReader reader, ILogger logger);
    }
}