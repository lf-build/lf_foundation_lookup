﻿using System;

namespace LendFoundry.Foundation.Lookup
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable("CONFIGURATION_NAME") ?? "lookup";
    }
}