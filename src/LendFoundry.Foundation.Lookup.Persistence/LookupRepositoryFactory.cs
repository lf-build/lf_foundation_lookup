﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.Foundation.Lookup.Persistence
{
    public class LookupRepositoryFactory : ILookupRepositoryFactory
    {
        public LookupRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public ILookupRepository Create(ITokenReader reader)
        {
            var tenantService = Provider.GetService<ITenantService>();
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var logger = Provider.GetService<ILogger>();
            var configuration = mongoConfigurationFactory.Get(reader, logger);

            return new LookupRepository(tenantService, configuration,logger);
        }
    }
}