﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Foundation.Lookup.Persistence
{
    public class LookupRepository : MongoRepository<ILookupEntry, LookupEntry>, ILookupRepository
    {
        private ILogger Logger { get; }

        static LookupRepository()
        {
            BsonClassMap.RegisterClassMap<LookupEntry>(map =>
            {
                map.AutoMap();
                var type = typeof(LookupEntry);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public LookupRepository(ITenantService tenantService, IMongoConfiguration configuration, ILogger logger) : base(tenantService, configuration, "lookup")
        {
            Logger = logger;
            CreateIndexIfNotExists
            (
               indexName: "entity-code",
               index: Builders<ILookupEntry>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Entity).Ascending(i => i.Code),
               unique: true
            );

            CreateIndexIfNotExists
            (
               indexName: "entity",
               index: Builders<ILookupEntry>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Entity)
            );
        }

        public IEnumerable<ILookupEntry> Add(string entity, IDictionary<string, string> lookupEntries)
        {
            var entries = new List<ILookupEntry>();
            var existingEntity = Query.Any(q => q.Entity == entity);
            if (existingEntity)
            {
                var codes = lookupEntries.Keys.Select(c => c).ToList();
                var existingCodes = codes.Any(c => Query.Where(q => q.Entity == entity).Any(x => x.Code == c));
                if (existingCodes)
                    throw new InvalidOperationException($"Already exists some Code inside database with Entity: #{entity}");
            }

            entries.AddRange(lookupEntries.Select((l, i) =>
            {
                var entry = new LookupEntry { Entity = entity, Code = l.Key, Value = l.Value };
                Add(entry);
                return entry;
            }));

            return entries;
        }

        public bool Delete(string entity, string code)
        {
            var tenantId = TenantService.Current.Id;

            return Collection.FindOneAndDeleteAsync(q =>
                q.TenantId == tenantId &&
                q.Entity == entity &&
                q.Code == code).Result != null;
        }

        public bool DeleteAll(string entity)
        {
            var tenantId = TenantService.Current.Id;

            var result = Collection.DeleteManyAsync(q =>
                q.TenantId == tenantId &&
                q.Entity == entity).Result;

            return result.DeletedCount > 0;
        }

        public IEnumerable<ILookupEntry> GetLookupEntries(string entity)
        {
            return Query.Where(l => l.Entity == entity).ToList();
        }

        public ILookupEntry GetLookupEntry(string entity, string code)
        {                                     
            var startedAt = DateTime.Now;
            var result=Query.FirstOrDefault(l => l.Entity == entity && l.Code == code);
            var finishedAt = DateTime.Now;
            Logger.Debug("Time taken for lookup entry", new {startedAt, finishedAt}, false);
            return result;
        }

        public List<string> GetEntities()
        {
            return Query.Select(l => l.Entity).Distinct().ToList();
        }
    }
}