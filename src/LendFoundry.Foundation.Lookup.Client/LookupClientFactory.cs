﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Foundation.Lookup.Client
{
    public class LookupClientFactory : ILookupClientFactory
    {
        [Obsolete("Need to use the overloaded constructor with Uri")]
        public LookupClientFactory(IServiceProvider provider, string endpoint, int port, int cachingExpirationInSeconds = 60)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
            CachingExpirationInSeconds = cachingExpirationInSeconds;
        }

        public LookupClientFactory(IServiceProvider provider, Uri uri = null, int cachingExpirationInSeconds = 60)
        {
            Provider = provider;
            Uri = uri;
            CachingExpirationInSeconds = cachingExpirationInSeconds;
        }

        private IServiceProvider Provider { get; set; }
        private int CachingExpirationInSeconds { get; }
        private Uri Uri { get; }

        public ILookupService Create(ITokenReader reader)
        {
            var tokenHandler = Provider.GetService<ITokenHandler>();
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("lookup");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new LookupServiceClient(client, reader, tokenHandler, CachingExpirationInSeconds);
        }
    }
}