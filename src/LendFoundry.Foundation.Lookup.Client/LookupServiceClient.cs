﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.Caching.Memory;
#else
using Microsoft.Framework.Caching.Memory;
# endif
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;

namespace LendFoundry.Foundation.Lookup.Client
{
    public class LookupServiceClient : ILookupService
    {
        private static readonly MemoryCache Cache = new MemoryCache(new MemoryCacheOptions());

        public LookupServiceClient(IServiceClient client, ITokenReader tokenReader, ITokenHandler tokenHandler, int cachingExpirationInSeconds = 60)
        {
            Client = client;
            TokenReader = tokenReader;
            TokenHandler = tokenHandler;
            CachingExpirationInSeconds = cachingExpirationInSeconds;
        }

        private IServiceClient Client { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenHandler { get; }
        private int CachingExpirationInSeconds { get; }

        public IEnumerable<ILookupEntry> Add(string entity, IDictionary<string, string> lookupEntries)
        {
            var request = new RestRequest("/{entity}", Method.POST);
            request.AddUrlSegment(nameof(entity), entity);
            request.AddJsonBody(lookupEntries);

            return Client.Execute<IEnumerable<LookupEntry>>(request);
        }

        public void Delete(string entity, string code)
        {
            var request = new RestRequest("/{entity}/{code}", Method.DELETE);
            request.AddUrlSegment(nameof(entity), entity);
            request.AddUrlSegment(nameof(code), code);

            Client.Execute(request);
        }

        public void DeleteAll(string entity)
        {
            var request = new RestRequest("/{entity}", Method.DELETE);
            request.AddUrlSegment(nameof(entity), entity);

            Client.Execute(request);
        }

        public List<string> GetEntities()
        {
            return GetFromCacheOtherwise("all", () =>
            {
                var request = new RestRequest("/entities", Method.GET);
                return Client.Execute<List<string>>(request);
            });
        }

        public Dictionary<string, string> GetLookupEntries(string entity)
        {
            return GetFromCacheOtherwise($"{entity}", () =>
            {
                var request = new RestRequest("/{entity}", Method.GET);
                request.AddUrlSegment(nameof(entity), entity);

                return Client.Execute<Dictionary<string, string>>(request);
            });
        }

        public Dictionary<string, string> GetLookupEntry(string entity, string code)
        {
            return GetFromCacheOtherwise($"{entity}{code}", () => {
                var request = new RestRequest("/{entity}/{code}", Method.GET);
                request.AddUrlSegment(nameof(entity), entity);
                request.AddUrlSegment(nameof(code), code);

                return Client.Execute<Dictionary<string, string>>(request);
            });
        }

        public Dictionary<string, IEnumerable<ILookupEntry>> GetMultipleLookupEntries(List<string> entities)
        {
            var rest = new RestRequest("/entities", Method.GET);
            AppendEntities(rest, entities);
            return Client.Execute<Dictionary<string, IEnumerable<ILookupEntry>>>(rest);
        }

        private static void AppendEntities(IRestRequest request, IReadOnlyList<string> entities)
        {
            if (entities == null || !entities.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < entities.Count; index++)
            {
                var tag = entities[index];
                url = url + $"/{{name{index}}}";
                request.AddUrlSegment($"name{index}", tag);
            }
            request.Resource = url;
        }

        public bool Exist(string entity, string code)
        {
            return GetFromCacheOtherwise($"{entity}{code}.exist", () => {
                var request = new RestRequest("/{entity}/{code}/exist", Method.GET);
                request.AddUrlSegment(nameof(entity), entity);
                request.AddUrlSegment(nameof(code), code);

                var response = Client.ExecuteRequest(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return string.Equals(response.Content.Trim(), "true", StringComparison.InvariantCultureIgnoreCase);
                }
                return false;
            });
        }


        private TResponse GetFromCacheOtherwise<TResponse>(string key, Func<TResponse> function)
        {
            lock (Cache)
            {
                var tokenRaw = TokenReader.Read();
                if (string.IsNullOrWhiteSpace(tokenRaw))
                    throw new AuthenticationException("Invalid token");
                var token = TokenHandler.Parse(tokenRaw);
                if (token == null)
                    throw new AuthenticationException("Invalid token");

                key = token.Tenant + "/" + key.TrimStart('/');
                TResponse response;
                if (!Cache.TryGetValue(key, out response))
                {
                    response = function();
                    if (CachingExpirationInSeconds > 0)
                    {
                        Cache.Set(key, response, new MemoryCacheEntryOptions
                        {
                            SlidingExpiration = TimeSpan.FromSeconds(CachingExpirationInSeconds)
                        });
                    }
                }
                return response;
            }
        }

    }

}