﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Foundation.Lookup.Client
{
    public static class LookupClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddLookupService(this IServiceCollection services, string endpoint, int port=500, int cachingExpirationInSeconds = 60)
        {
            services.AddTransient<ILookupClientFactory>(p => new LookupClientFactory(p, endpoint, port,cachingExpirationInSeconds));
            services.AddTransient(p => p.GetService<ILookupClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddLookupService(this IServiceCollection services, Uri uri, int cachingExpirationInSeconds = 60)
        {
            services.AddTransient<ILookupClientFactory>(p => new LookupClientFactory(p, uri, cachingExpirationInSeconds));
            services.AddTransient(p => p.GetService<ILookupClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddLookupService(this IServiceCollection services, int cachingExpirationInSeconds = 60)
        {
            services.AddTransient<ILookupClientFactory>(p => new LookupClientFactory(p, null, cachingExpirationInSeconds));
            services.AddTransient(p => p.GetService<ILookupClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}