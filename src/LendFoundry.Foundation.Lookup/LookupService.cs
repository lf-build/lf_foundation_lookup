﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Foundation.Lookup
{
    public class LookupService : ILookupService
    {
        public LookupService(ILookupRepository repository, ILogger logger)
        {
            Repository = repository;
            Logger = logger;
        }

        private ILookupRepository Repository { get; }

        private ILogger Logger { get; }

        public IEnumerable<ILookupEntry> Add(string entity, IDictionary<string, string> lookupEntries)
        {
            entity = EnsureEntity(entity);

            if (lookupEntries == null)
                throw new ArgumentException($"#{nameof(lookupEntries)} cannot be null");

            if (lookupEntries.Count == 0)
                throw new ArgumentException($"#{nameof(lookupEntries)} cannot be empty");

            return Repository.Add(entity, lookupEntries);
        }

        public void Delete(string entity, string code)
        {
            entity = EnsureEntity(entity);

            if (string.IsNullOrWhiteSpace(code))
                throw new ArgumentException($"#{nameof(code)} cannot be null");

            if (Repository.Delete(entity, code))
                Logger.Info($"LookupEntry - {{ Entity: #{entity}, Code: #{code} }} was removed from database");
            else
                throw new NotFoundException($"The LookupEntry with {{ Entity: #{entity}, Code: #{code} }} was not found");

        }

        public void DeleteAll(string entity)
        {
            entity = EnsureEntity(entity);

            if (Repository.DeleteAll(entity))
                Logger.Info($"All LookupEntries related to {{ Entity: #{entity} }} was removed from database");
            else
                throw new NotFoundException($"The LookupEntries related to {{ Entity: #{entity} }} was not found");
        }

        public Dictionary<string, string> GetLookupEntries(string entity)
        {
            entity = EnsureEntity(entity);

            var lookupEntries = new Dictionary<string, string>();

            Repository.GetLookupEntries(entity).ToList().ForEach((l) =>
            {
                lookupEntries.Add(l.Code, l.Value);
            });

            if (lookupEntries.Count == 0)
                throw new NotFoundException($"The LookupEntries related to {{ Entity: #{entity} }} was not found");

            return lookupEntries;
        }

        public Dictionary<string, IEnumerable<ILookupEntry>> GetMultipleLookupEntries(List<string> entities)
        {
            if (entities == null || !entities.Any())
                throw new ArgumentException("At least one entities required", nameof(entities));
            var lookupEntries = new Dictionary<string, IEnumerable<ILookupEntry>>();
            foreach (var item in entities)
            {
                var entity = EnsureEntity(item);
                lookupEntries.Add(item, Repository.GetLookupEntries(entity));
            }
            return lookupEntries;
        }

        public Dictionary<string, string> GetLookupEntry(string entity, string code)
        {
            entity = EnsureEntity(entity);

            if (string.IsNullOrWhiteSpace(code))
                throw new ArgumentException($"#{nameof(code)} cannot be null");

            var lookupEntries = new Dictionary<string, string>();
            var lookupEntry = Repository.GetLookupEntry(entity, code);

            if (lookupEntry == null)
                throw new NotFoundException($"The LookupEntry with {{ Entity: #{entity}, Code: #{code} }} was not found");

            lookupEntries.Add(lookupEntry.Code, lookupEntry.Value);
            return lookupEntries;

        }

        public List<string> GetEntities()
        {
            return Repository.GetEntities();
        }

        private string EnsureEntity(string entity)
        {
            if (string.IsNullOrWhiteSpace(entity))
                throw new ArgumentException($"#{nameof(entity)} cannot be null");

            entity = entity.ToLower();
            return entity;
        }

        public bool Exist(string entity, string code)
        {
            entity = EnsureEntity(entity);

            if (string.IsNullOrWhiteSpace(code))
                throw new ArgumentException($"#{nameof(code)} cannot be null");

            var lookupEntry = Repository.GetLookupEntry(entity, code);

            if (lookupEntry == null)
                return false;

            return true;
        }
    }
}